import unittest
import sys 
sys.path.append('../src')
from roman_numeral import to_roman_numeral

class TestRomanNumeral(unittest.TestCase):

		def test_zero_should_return_empty_string(self):                
			self.assertEqual("", to_roman_numeral(0))
				
		def testNumber3ShouldReturnRomanIII(self):
			self.assertEqual("III",to_roman_numeral(3));    		
				
		def testNumber4ShouldReturnRomanIV(self):       
			self.assertEqual("IV",to_roman_numeral(4));   				
		
		def testNumber5ShouldReturnRomanV(self):       
			self.assertEqual("V",to_roman_numeral(5));   
		
		def testNumber8ShouldReturnRomanVIII(self):       
			self.assertEqual("VIII",to_roman_numeral(8));   
		
		def testNumber9ShouldReturnRoman(self):       
			self.assertEqual("XCI",to_roman_numeral(91));   
		
		def testNumber35ShouldReturnRomanXXXV(self):
			self.assertEqual("XXXV",to_roman_numeral(35));    
		
		def testNumber40ShouldReturnRomanXL(self):       
			self.assertEqual("XL",to_roman_numeral(40));   
		
		def testNumber50ShouldReturnRomanL(self):       
			self.assertEqual("L",to_roman_numeral(50));   
		
		def testNumber89ShouldReturnRomanLXXXIX(self):       
			self.assertEqual("LXXXIX",to_roman_numeral(89));   
		
		def testNumber90ShouldReturnRomanXC(self):       
			self.assertEqual("XC",to_roman_numeral(90));   
		
		def testNumber3999ShouldReturnRomanMMMCMXCIX(self):       
			self.assertEqual("MMMCMXCIX",to_roman_numeral(3999));   
			
if __name__ == '__main__':
    unittest.main()