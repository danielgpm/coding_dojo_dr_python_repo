#! /usr/bin/env python
#------------------------------------------------------------------------------------ 
# Author: Daniel Paniagua
#------------------------------------------------------------------------------------

roman_map = { 0: ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" ], 
							1: [ "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"],
							2: ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"],
							3: ["", "M", "MM", "MMM"]
						}
	
def to_roman_numeral(number):
	num_str = str(number)
	l = len(num_str) - 1
	romans = []
	if number >= 0 and number < 4000:
		for idx, c in enumerate(num_str):
			romans.append(roman_map[ l - idx][int(c)])
	return ''.join(romans)
	